from datetime import date
from pydantic import BaseModel, validator, ValidationError
from enum import Enum
from email_validator import validate_email 


class Team(BaseModel):
    id: int | None = None
    name: str


class Player(BaseModel):
    id: int | None = None
    first_name: str | None = None
    second_name: str | None = None
    country: str | None = None
    team: str | None = None


    @classmethod
    def from_query_result(cls, id, first_name, second_name, country, team):
        return cls(
            id = id,
            first_name = first_name,
            second_name = second_name,
            country = country,
            team = team)



class Role(str, Enum):
    adm = "admin"
    usr = "user"
    drc = "director"


class Sort(str, Enum):
    asc = "asc"
    desc = "desc"


class User(BaseModel):
    id: int | None = None
    email: str
    password: str
    role: Role | None = None
    associated_player: int | None = None

    @validator('email')
    def email_is_valid(cls, email):
        try:
            validate_email(email)
            return email
        except: raise TypeError("Invalid email")
    
    @classmethod
    def from_query_result(cls, id, email, password, role, associated_player):
        return cls(
            id = id,
            email = email,
            password = password,
            role = role,
            associated_player = associated_player
        )
    
    def is_admin(self):
        return self.role == "admin"
    
    def is_director(self):
        return self.role == "director"


class LogInfo(BaseModel):
    email: str
    password: str

    @validator('email')
    def email_is_valid(cls, email):
        try:
            validate_email(email)
            return email
        except: raise TypeError("Invalid email")


class Status(str, Enum):
    pending = "pending"
    denied = "denied"
    accepted = "accepted"


class PromotionRequest(BaseModel):
    id: int | None = None
    user_id: int
    sent_at: date
    status: Status
    admin_id: int | None = None

    @classmethod
    def from_query_result(cls, id, user_id, sent_at, status, admin_id):
        return cls(
            id = id,
            user_id = user_id,
            sent_at = sent_at,
            status = status,
            admin_id = admin_id
        )


class LinkRequest(BaseModel):
    id: int | None = None
    user_id: int
    player_id: int | None = None
    sent_at: date
    status: Status
    admin_id: int | None = None

    @classmethod
    def from_query_result(cls, id, user_id, player_id, sent_at, status, admin_id):
        return cls(
            id = id,
            user_id = user_id,
            player_id = player_id,
            sent_at = sent_at,
            status = status,
            admin_id = admin_id
        )


class LinkedUserDetails(BaseModel):
    user_id: int
    user_email: str
    player: Player

    @validator('user_email')
    def email_is_valid(cls, user_email):
        try:
            validate_email(user_email)
            return user_email
        except: raise TypeError("Invalid email")


#===================================== Match


class Match(BaseModel):
    id: int | None = None
    title: str | None = None
    played_at: date = None
    match_format_id: int | None



    @classmethod
    def from_query_result(cls, id, title, played_at, match_format_id):
        return cls(
            id = id,
            title = title,
            played_at = played_at,
            match_format_id = match_format_id)


class PlayerMatch(BaseModel):
    match_id: int
    title: str
    played_at: date
    match_format_id: int
    player_id: list[int]
    player_name: list[str]


    @classmethod
    def from_query_result(cls, match_id, title, played_at, match_format_id, player_id, player_name):
        return cls(
            match_id = match_id,
            title = title,
            played_at = played_at,
            match_format_id = match_format_id,
            player_id=player_id,
            player_name=player_name)


class TeamMatch(BaseModel):
    match_id: int
    title: str
    played_at: date
    match_format_id: int
    team_id: list[int]
    team_name: list[str]



    @classmethod
    def from_query_result(cls, match_id, title, played_at, match_format_id, team_id, team_name):
        return cls(
            match_id = match_id,
            title = title,
            played_at = played_at,
            match_format_id = match_format_id,
            team_id=team_id,
            team_name=team_name)



class MatchResponseModel(BaseModel):
    id: int | None = None
    title: str | None = None
    played_at: date
    match_format_id: int



class PlayerMatchRequest(BaseModel):
    id: int | None
    title: str
    played_at: date
    match_format_id: int | None
    player_ids: list[int]


class PlayerMatchInfo(BaseModel):
    player_id: int
    player_name: str
    country_id: int = None
    score: str


    @classmethod
    def from_query_result(cls,player_id, player_name, country_id, score):
        return cls(
            player_id=player_id,
            player_name=player_name,
            country_id=country_id,
            score=score)



class TeamMatchInfo(BaseModel):
    team_id: int
    team_name: str
    score: str
    

    @classmethod
    def from_query_result(cls,team_id, team_name, score):
        return cls(
            team_id=team_id,
            team_name=team_name,
            score=score)


class TeamMatchRequest(BaseModel):
    id: int | None
    title: str
    played_at: date = None
    match_format_id: int | None
    team_ids: list[int]


class MatchUpdate(BaseModel):
    title: str | None
    played_at: date


class MatchFormat(BaseModel):
    id: int | None
    name: str
    rules: str


    @classmethod
    def from_query_result(cls, id, name, rules):
        return cls(
            id=id,
            name=name,
            rules=rules)


class MatchFormatUpdate(BaseModel):
    name: str
    rules: str


class PlayerMatchDetailUpdate(BaseModel):
    player_ids: list[int]
    score: list[int]


class TeamMatchDetailUpdate(BaseModel):
    team_ids: list[int]
    score: list[int]



#============== Tournament ============= 
class MatchUp(BaseModel):
    id: int | None = None
    tournament_id: int | None = None
    played_at: date | None = None
    tournament_phase: int | None = None
    player_one: int | None = None
    player_two: int | None = None
    player_one_score: int | None = None
    player_two_score: int | None = None


class Tournament(BaseModel):
    id: int | None = None
    title: str
    prize: str | None = None
    format_id: int | None = None
    winner: int | None = None
    players: list[Player] | None = []
    matchups:  list[MatchUp] | None = []
    
    
    @classmethod
    def from_query_result(cls, id, title, prize, format_id, winner, players, matchups):
        return cls(
            id = id,
            title = title,
            prize = prize,
            format_id = format_id,
            winner = winner,
            players = players,
            matchups = matchups)


class All_Tournaments(BaseModel):
    id: int | None
    title: str
    prize: str | None
    format_id: int
    winner: int | None
    players: list[int] | None = []
    matchups:  list[int] | None = []
    
    
    @classmethod
    def from_query_result(cls, id, title, prize, format_id, winner, players, matchups):
        return cls(
            id = id,
            title = title,
            prize = prize,
            format_id = format_id,
            winner = winner,
            players = players,
            matchups = matchups)


class TournamentResponseModel(BaseModel):
    id: int | None
    title: str
    prize: str | None
    format: str
    winner: int | None
    players: list[Player] | None = []
    matchups:  list[MatchUp] | None = []


class Scores(BaseModel):
    score_one: int
    score_two: int


class LeagueStanding(BaseModel):
    player: Player
    points: int


class Order(str, Enum):
    ascending = "asc"
    descending = "desc"


class PlayerStatistics(BaseModel):
    player: Player
    tournaments_played: list[str] | None = []
    tournaments_won: list[str] | None = []
    matches_played: list[str] | None = []
    matches_won: list[int] | None = []
    most_often_played_oponent: list[int] | None = []
    win_percentage: list[float] | None = []
    lose_percentage: list[float] | None = []


class Participants(BaseModel):
    participants: list[str]